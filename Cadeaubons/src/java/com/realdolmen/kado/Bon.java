/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.kado;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author YVDBL89
 */
@Entity
@Table(name = "BON")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bon.findAll", query = "SELECT b FROM Bon b")
    , @NamedQuery(name = "Bon.findById", query = "SELECT b FROM Bon b WHERE b.id = :id")
    , @NamedQuery(name = "Bon.findByBonnr", query = "SELECT b FROM Bon b WHERE b.bonnr = :bonnr")
    , @NamedQuery(name = "Bon.findByNaam", query = "SELECT b FROM Bon b WHERE b.naam = :naam")
    , @NamedQuery(name = "Bon.findByInformatie", query = "SELECT b FROM Bon b WHERE b.informatie = :informatie")
    , @NamedQuery(name = "Bon.findByLeverancier", query = "SELECT b FROM Bon b WHERE b.leverancier = :leverancier")
    , @NamedQuery(name = "Bon.findByPrijs", query = "SELECT b FROM Bon b WHERE b.prijs = :prijs")
    , @NamedQuery(name = "Bon.findByGeldigTot", query = "SELECT b FROM Bon b WHERE b.geldigTot = :geldigTot")})
public class Bon implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "BONNR")
    private String bonnr;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAAM")
    private String naam;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "INFORMATIE")
    private String informatie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "LEVERANCIER")
    private String leverancier;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRIJS")
    private BigDecimal prijs;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GELDIG_TOT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date geldigTot;
    @JoinColumn(name = "THEMAID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Thema themaid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bonid")
    private Collection<Bestelling> bestellingCollection;

    public Bon() {
    }

    public Bon(BigDecimal id) {
        this.id = id;
    }

    public Bon(BigDecimal id, String bonnr, String naam, String informatie, String leverancier, BigDecimal prijs, Date geldigTot) {
        this.id = id;
        this.bonnr = bonnr;
        this.naam = naam;
        this.informatie = informatie;
        this.leverancier = leverancier;
        this.prijs = prijs;
        this.geldigTot = geldigTot;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getBonnr() {
        return bonnr;
    }

    public void setBonnr(String bonnr) {
        this.bonnr = bonnr;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getInformatie() {
        return informatie;
    }

    public void setInformatie(String informatie) {
        this.informatie = informatie;
    }

    public String getLeverancier() {
        return leverancier;
    }

    public void setLeverancier(String leverancier) {
        this.leverancier = leverancier;
    }

    public BigDecimal getPrijs() {
        return prijs;
    }

    public void setPrijs(BigDecimal prijs) {
        this.prijs = prijs;
    }

    public Date getGeldigTot() {
        return geldigTot;
    }

    public void setGeldigTot(Date geldigTot) {
        this.geldigTot = geldigTot;
    }

    public Thema getThemaid() {
        return themaid;
    }

    public void setThemaid(Thema themaid) {
        this.themaid = themaid;
    }

    @XmlTransient
    public Collection<Bestelling> getBestellingCollection() {
        return bestellingCollection;
    }

    public void setBestellingCollection(Collection<Bestelling> bestellingCollection) {
        this.bestellingCollection = bestellingCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bon)) {
            return false;
        }
        Bon other = (Bon) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.kado.Bon[ id=" + id + " ]";
    }
    
}
