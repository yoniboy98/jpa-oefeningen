/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.kado;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author YVDBL89
 */
@Entity
@Table(name = "THEMA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Thema.findAll", query = "SELECT t FROM Thema t")
    , @NamedQuery(name = "Thema.findById", query = "SELECT t FROM Thema t WHERE t.id = :id")
    , @NamedQuery(name = "Thema.findByThema", query = "SELECT t FROM Thema t WHERE t.thema = :thema")})
public class Thema implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "THEMA")
    private String thema;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "themaid")
    private Collection<Bon> bonCollection;

    public Thema() {
    }

    public Thema(BigDecimal id) {
        this.id = id;
    }

    public Thema(BigDecimal id, String thema) {
        this.id = id;
        this.thema = thema;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getThema() {
        return thema;
    }

    public void setThema(String thema) {
        this.thema = thema;
    }

    @XmlTransient
    public Collection<Bon> getBonCollection() {
        return bonCollection;
    }

    public void setBonCollection(Collection<Bon> bonCollection) {
        this.bonCollection = bonCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Thema)) {
            return false;
        }
        Thema other = (Thema) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.realdolmen.kado.Thema[ id=" + id + " ]";
    }
    
}
