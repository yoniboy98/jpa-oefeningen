/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.netbeans;

import javax.persistence.*;

/**
 *
 * @author YVDBL89
 */
@Entity
public class Point {
    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private String name;



    
    private int x;
    private int y;

    public Point(int x, int y) {
     
        this.x = x;
        this.y = y;
    }
public Point() {
    
}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Point{" + "id=" + id + ", name=" + name + ", x=" + x + ", y=" + y + '}';
    }
  
    
    
    
  
}
