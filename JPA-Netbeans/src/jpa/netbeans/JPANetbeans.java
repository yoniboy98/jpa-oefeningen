/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.netbeans;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author YVDBL89
 */
public class JPANetbeans {

    public static void main(String[] args) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("$objectdb/db/p2.odb");
       EntityManager em = emf.createEntityManager();
       em.getTransaction().begin();
       
       for(int i=0; i<1000; i++){
           Point point = new Point(i, i);
           em.persist(point);
       }
       em.getTransaction().commit();
       
      TypedQuery<Point> query = em.createQuery("SELECT point FROM Point point", Point.class);
        List<Point> results = query.getResultList();
        for(Point p : results) {
            System.out.println(p);
            
                    
        }
        
        
        em.close();
        emf.close();
        
        
        
    }
    
}
